package com.example.zal.michal158988.domain158988;

import com.example.zal.michal158988.domain158988.dto158988.PersonDto158988;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class FileManager158988 {

    private String txtPath = "src/main/resources/plikTekstowy158988.txt";
    private String csvPath = "src/main/resources/plikTabelaryczny158988.csv";

    FileManager158988() {
        try {
            new File(txtPath).createNewFile();
            new File(csvPath).createNewFile();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    void writeToTxt(Person158988 person) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(txtPath, true));
             BufferedReader reader = new BufferedReader(new FileReader(txtPath))) {
            if (reader.readLine() == null) {
                writer.write(person.toString());
            } else {
                writer.newLine();
                writer.write(person.toString());
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    void writeToCsv(Person158988 person) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvPath, true));
             BufferedReader reader = new BufferedReader(new FileReader(csvPath))) {
            if (reader.readLine() == null) {
                writer.write("ID,First Name,Last Name,Age");
                writer.newLine();
            }
            writer.write(person.getId() + "," + person.getFirstName() + "," + person.getLastName() + "," + person.getAge());
            writer.newLine();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    List<PersonDto158988> readFromCsv() {
        List<PersonDto158988> persons = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(csvPath))) {
            String line;
            boolean isFirstLine = true;
            while ((line = reader.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue; // Skip header line
                }
                String[] fields = line.split(",");
                if (fields.length == 4) {
                    PersonDto158988 person = new PersonDto158988(fields[0], fields[1], fields[2], Integer.parseInt(fields[3]));
                    persons.add(person);
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return persons;
    }

    byte[] getCsvFileContent() {
        try {
            return Files.readAllBytes(Paths.get(csvPath));
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return new byte[0];
        }
    }

    void methodForCherryPick() {
        System.out.println("Simulating cherry picking");
    }

}
