package com.example.zal.michal158988.domain158988;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonManagerRepository158988 {

    Person158988 save(Person158988 person);

    List<Person158988> findAllPersons();

    Optional<Person158988> findById(String id);

    void delete(String id);

    Person158988 update(Person158988 person158988);

}
