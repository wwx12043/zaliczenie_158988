package com.example.zal.michal158988.domain158988;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Person158988 {

    private String id;

    private String firstName;

    private String lastName;

    private int age;

}
