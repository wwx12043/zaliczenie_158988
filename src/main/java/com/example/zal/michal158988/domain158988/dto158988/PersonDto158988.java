package com.example.zal.michal158988.domain158988.dto158988;

import lombok.*;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto158988 {

    private String id;

    private String firstName;

    private String lastName;

    private int age;

}
