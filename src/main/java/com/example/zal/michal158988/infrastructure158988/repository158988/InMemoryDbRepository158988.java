package com.example.zal.michal158988.infrastructure158988.repository158988;

import com.example.zal.michal158988.domain158988.Person158988;
import com.example.zal.michal158988.domain158988.PersonManagerRepository158988;
import com.example.zal.michal158988.domain158988.exceptions158988.PersonNotFoundException158988;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class InMemoryDbRepository158988 implements PersonManagerRepository158988 {

    Map<String, Person158988> inMemoryDb158988 = new HashMap<>();

    @Override
    public Person158988 save(Person158988 person) {
        inMemoryDb158988.put(person.getId(), person);
        return person;
    }

    @Override
    public List<Person158988> findAllPersons() {
        return new ArrayList<>(inMemoryDb158988.values());
    }

    @Override
    public Optional<Person158988> findById(String id) {
        return Optional.ofNullable(inMemoryDb158988.get(id));
    }

    @Override
    public void delete(String id) {
        inMemoryDb158988.remove(id);
    }

    @Override
    public Person158988 update(Person158988 person) {
        if (inMemoryDb158988.containsKey(person.getId())) {
            inMemoryDb158988.put(person.getId(), person);
            return person;
        } else {
            throw new PersonNotFoundException158988("Person with ID " + person.getId() + " not found");
        }
    }
}
