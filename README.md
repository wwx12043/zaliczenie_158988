## Uruchomienie lokalnie

Sklonuj projekt

```bash
  git clone https://gitlab.com/wwx12043/zaliczenie_158988.git
```

Przejdź do głównego katalogu

```bash
  cd project-directory
```

Uruchom aplikację

```bash
  ./mvnw spring-boot:run
```

Otwórz przeglądarkę internetową i przejdź pod adres

a) aby uzyskać dostęp do interfejsu użytkownika
```bash
  http://localhost:8080/158988/app
```
b) aby uzyskać dostęp do dokumentacji API
```bash
http://localhost:8080/swagger-ui/index.html#/person-controller-158988
```